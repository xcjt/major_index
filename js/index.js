$(function () {
    // 导航切换
    $(".nav-list").on('click', ".item", function () {
        $(this).parent().find('.active').removeClass("active");
        $(this).addClass("active");
    })

    // case板块切换
    $("#case-box").on("click", '.item', function () {
        $("#icon-panel").attr("src", $(this).attr("src").replace("black", "white"))
    })

    var mySwiper = new Swiper('.swiper-container', {
        autoplay: true,//可选选项，自动滑动
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        grabCursor: true,
    })

    document.getElementById('header').style.setProperty('height', document.documentElement.clientHeight + "px")

    new WOW().init({
        offset: -30
    });

})